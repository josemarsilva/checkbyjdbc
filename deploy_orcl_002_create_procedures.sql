-- ----------------------------------------------------------------------------
-- File Name    : deploy_orcl_002_create_procedures.sql
-- Description  : Sample procedure for check_by_jdbc
-- ----------------------------------------------------------------------------

spool deploy_orcl_002_create_procedures.log

create or replace procedure NAGIOS.check_by_jdbc_sample
  (
    num_ret         IN OUT NUMBER,
    str_ret         IN OUT VARCHAR2,
    w_param         IN NUMBER,
    c_param         IN NUMBER,
    str_adhoc_param IN VARCHAR2
  ) is
  --
  -- Nagios return constants ...
  --
  NAGIOS_RETURN_OK       NUMBER := 0;
  NAGIOS_RETURN_WARNING  NUMBER := 1;
  NAGIOS_RETURN_CRITICAL NUMBER := 2;
  NAGIOS_RETURN_UNKNOWN  NUMBER := 3;  
  --
begin
  --
  -- Put your code here! Your code should evaluate something, considering "w"arning, "c"ritical 
  -- parameters and "str_adhoc_param" a string ad-hoc parameter and return on "num_ret" Nagios 
  -- STATUS convention and "str_ret" Nagios STATUS return string
  --
  if str_adhoc_param = to_char(NAGIOS_RETURN_OK) then
    num_ret := NAGIOS_RETURN_OK;
  elsif str_adhoc_param = to_char(c_param) then
    num_ret := NAGIOS_RETURN_CRITICAL;
  elsif str_adhoc_param = to_char(w_param) then
    num_ret := NAGIOS_RETURN_WARNING;
  else
    num_ret := NAGIOS_RETURN_UNKNOWN;
  end if;
  --
  str_ret := 'CheckByJdbc-PL/SQL procedure: Your input parameter value was "' || str_adhoc_param || '". I have evaluated this and the return to Nagios is ' || num_ret || '!' ;
  --
end;
/
show errors
