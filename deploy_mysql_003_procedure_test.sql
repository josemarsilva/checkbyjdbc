/*
 * ----------------------------------------------------------------------------
 * File Name    : deploy_mysql_003_procedure_test.sql
 * Description  : 
 * ----------------------------------------------------------------------------
 */

# mysql -u nagios -pnagios;
mysql> use nagios_dbtest;


SET @num_ret = NULL;
SET @str_ret = NULL;
SET @w_param = 1;
SET @c_param = 2;
SET @str_adhoc_param = '1';
CALL check_by_jdbc_sample( @num_ret, @str_ret, @w_param, @c_param, @str_adhoc_param );

SELECT '@num_ret', @num_ret
UNION ALL
SELECT '@str_ret', @str_ret
UNION ALL
SELECT '@w_param', @w_param
UNION ALL
SELECT '@c_param', @c_param
UNION ALL
SELECT '@str_adhoc_param', @str_adhoc_param;


SET @num_ret = NULL;
SET @str_ret = NULL;
SET @w_param = 1;
SET @c_param = 2;
SET @str_adhoc_param = '2';
CALL check_by_jdbc_sample( @num_ret, @str_ret, @w_param, @c_param, @str_adhoc_param );

SELECT '@num_ret', @num_ret
UNION ALL
SELECT '@str_ret', @str_ret
UNION ALL
SELECT '@w_param', @w_param
UNION ALL
SELECT '@c_param', @c_param
UNION ALL
SELECT '@str_adhoc_param', @str_adhoc_param;

SET @num_ret = NULL;
SET @str_ret = NULL;
SET @w_param = 1;
SET @c_param = 2;
SET @str_adhoc_param = '0';
CALL check_by_jdbc_sample( @num_ret, @str_ret, @w_param, @c_param, @str_adhoc_param );

SELECT '@num_ret', @num_ret
UNION ALL
SELECT '@str_ret', @str_ret
UNION ALL
SELECT '@w_param', @w_param
UNION ALL
SELECT '@c_param', @c_param
UNION ALL
SELECT '@str_adhoc_param', @str_adhoc_param;
