declare
  --
  v_num_ret         NUMBER;
  v_str_ret         VARCHAR2(1000);
  v_w_param         NUMBER := 1;
  v_c_param         NUMBER := 2;
  v_str_adhoc_param VARCHAR2(100);
  --
begin
  --
  dbms_output.put_line('-----------------------------------');
  dbms_output.put_line('turn 0' );
  v_str_adhoc_param := '0';
  NAGIOS.check_by_jdbc_sample
  (
    num_ret         => v_num_ret,
    str_ret         => v_str_ret,
    w_param         => v_w_param,
    c_param         => v_c_param,
    str_adhoc_param => v_str_adhoc_param
  );
  --
  dbms_output.put_line('str_adhoc_param = ' || v_str_adhoc_param);
  dbms_output.put_line('num_ret         = ' || v_num_ret);
  dbms_output.put_line('str_ret         = ' || v_str_ret);
  dbms_output.put_line('w_param         = ' || v_w_param);
  dbms_output.put_line('c_param         = ' || v_c_param);
  --
  dbms_output.put_line('-----------------------------------');
  v_str_adhoc_param := '1';
  NAGIOS.check_by_jdbc_sample
  (
    num_ret         => v_num_ret,
    str_ret         => v_str_ret,
    w_param         => v_w_param,
    c_param         => v_c_param,
    str_adhoc_param => v_str_adhoc_param
  );
  dbms_output.put_line('turn 1' );
  dbms_output.put_line('str_adhoc_param = ' || v_str_adhoc_param);
  dbms_output.put_line('num_ret         = ' || v_num_ret);
  dbms_output.put_line('str_ret         = ' || v_str_ret);
  dbms_output.put_line('w_param         = ' || v_w_param);
  dbms_output.put_line('c_param         = ' || v_c_param);
  --
  dbms_output.put_line('-----------------------------------');
  v_str_adhoc_param := '2';
  NAGIOS.check_by_jdbc_sample
  (
    num_ret         => v_num_ret,
    str_ret         => v_str_ret,
    w_param         => v_w_param,
    c_param         => v_c_param,
    str_adhoc_param => v_str_adhoc_param
  );
  dbms_output.put_line('turn 2' );
  dbms_output.put_line('str_adhoc_param = ' || v_str_adhoc_param);
  dbms_output.put_line('num_ret         = ' || v_num_ret);
  dbms_output.put_line('str_ret         = ' || v_str_ret);
  dbms_output.put_line('w_param         = ' || v_w_param);
  dbms_output.put_line('c_param         = ' || v_c_param);
  --
  dbms_output.put_line('-----------------------------------');
  v_str_adhoc_param := '3';
  NAGIOS.check_by_jdbc_sample
  (
    num_ret         => v_num_ret,
    str_ret         => v_str_ret,
    w_param         => v_w_param,
    c_param         => v_c_param,
    str_adhoc_param => v_str_adhoc_param
  );
  dbms_output.put_line('str_adhoc_param = ' || v_str_adhoc_param);
  dbms_output.put_line('turn 3' );
  dbms_output.put_line('num_ret         = ' || v_num_ret);
  dbms_output.put_line('str_ret         = ' || v_str_ret);
  dbms_output.put_line('w_param         = ' || v_w_param);
  dbms_output.put_line('c_param         = ' || v_c_param);
  --
  dbms_output.put_line('-----------------------------------');
  v_str_adhoc_param := '';
  NAGIOS.check_by_jdbc_sample
  (
    num_ret         => v_num_ret,
    str_ret         => v_str_ret,
    w_param         => v_w_param,
    c_param         => v_c_param,
    str_adhoc_param => v_str_adhoc_param
  );
  dbms_output.put_line('str_adhoc_param = ' || v_str_adhoc_param);
  dbms_output.put_line('turn 4' );
  dbms_output.put_line('num_ret         = ' || v_num_ret);
  dbms_output.put_line('str_ret         = ' || v_str_ret);
  dbms_output.put_line('w_param         = ' || v_w_param);
  dbms_output.put_line('c_param         = ' || v_c_param);
  --
end;
       
