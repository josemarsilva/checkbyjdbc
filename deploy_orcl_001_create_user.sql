-- ----------------------------------------------------------------------------
-- File Name    : deploy_orcl_001_create_user.sql
-- Description  : Creates owner user of check database objects. Grant only 
--                necessary permission to user
-- ----------------------------------------------------------------------------

spool deploy_orcl_001_create_user.log

create user nagios identified by nagios;
grant  connect to nagios;
grant  create procedure to nagios;
grant select on dba_free_space to nagios;
grant select on dba_data_files to nagios;
grant select on dba_objects to nagios;
grant select on V_$SESSION to nagios /* SYNONYM V$SESSION */;
 
spool off
