# Check By JDBC Nagios Plugin #

## 1. Introduction ##

CheckByJdbc is a Nagios plugin that enables custom SQL-based service checks of several popular JDBC-compliant databases (Oracle, Microsoft SQL Server, MySQL, DB2 for i, DB2 for LUW, DB2 for z/OS, and HP Vertica). Written in Java, CheckByJdbc is a simple POJO that opens a database connection and executes a user-defined SQL query or stored procedure call. It evaluates the result set row(s) returned by the query to determine the service check status and construct the message text. 

The plugin obtains the SQL queries it executes from local text files. Each query file contains a single SQL statement (with no command terminator) that returns a result set (i.e. SELECT, VALUES, or, in some cases, CALL). The names of the columns in the result set do not matter, but their position and data type must adhere to the following convention:

  * The first column in the result set must be numeric value that corresponds to a Nagios STATUS code:
    - 0     : "OK" status
    - 1     : "WARNING" status
    - 2     : "CRITICAL" status
    - others: "UNKNOWN" status
  * The second column in the result contains character data that will be concatenated to the Nagios STATUS string, separated by commas ( <r1c2>; <r2c2>; ...; <r*n*c2> )
  * An empty result set will be reported as "UNKNOWN".
  * The query can return multiple rows. The plugin will iterate through the entire result set and return a final status that reflects the most severe status value it encountered ("CRITICAL", "WARN", "UNKNOWN", "OK"). 
  * Enabling verbose output via the -v flag will produce debug text on stdout that is not valid for Nagios, but will aid in troubleshooting the query or connectivity settings.
  * Queries run by CheckByJdbc cannot use traditional SQL question mark parameter markers, but they can refer to substitution variables (passed in on the command line via -R) that the plugin will expand into the SQL statement text before executing the query. In addition to user-defined substitution variables, the following variables are built-in and available for use in all queries:
    - &w : Will be replaced with the warning value passed in via the -w parameter  
    - &c : Will be replaced with the critical value passed in via the -c parameter

## 1.2. For developers who want to study and customize ##

If you are a developer and want to compile or adjust the source code, you must:

  * Download all files recursively from CheckByJdbc plugin from repository
  * Download [JSAP](http://www.martiansoftware.com/jsap/)-2.0a.jar (or newer) and unzip it into directory
  * Download the JDBC drivers for your database(s) and copy them into into CheckByJdbc directory. Any JDBC drivers included in this repository are likely outdated, so downloading new drivers from an official site is recommended.
    - Oracle: classes12.jar
    - MySQL: mysql-connector-java-5.1.18-bin
    - Microsoft SQL Server: 
        + sqljdbc4.jar (Windows)
        + sqljdbc_4.0.2206.100_enu.tar (Linux)
    - IBM DB2/400 (aka DB2 for i):
        + jt400.jar (http://jt400.sourceforge.net/)
    - HP Vertica:
        + vertica-jdbc-*x.x.x-x*.jar ([HP Vertica Client Drivers](http://www.vertica.com/resources/vertica-client-drivers/))
    - IBM DB2 for LUW, z/OS, and i (depending on license JAR):
        + db2jcc.jar ([DB2 JDBC Driver Versions and Downloads](http://www-01.ibm.com/support/docview.wss?uid=swg21363866)) 
        + db2jcc_license_cu.jar (or db2jcc_license_cisuz.jar for midrange or mainframe DB2 databases)

  * file 'makefile.bat' ( 'makefile.sh' for Linux) helps you to compile source code
  * All of the source code is in 'CheckByJdbc.java'
  * I've studied two other database plugins available on [Nagios Exchange](https://exchange.nagios.org/):
    - [check_db](https://exchange.nagios.org/directory/Plugins/Databases/Oracle/check_db/details)
    - [check_tablespace_oracle](https://exchange.nagios.org/directory/Plugins/Databases/Oracle/check_tablespace_oracle/details)

    Both were good plugins, but I wanted something that could run a custom SQL query and translate its result(s) into a Nagios status (OK, WARNING, CRITICAL, UNKNOWN). I also needed a plugin that could connect to other database platforms besides Oracle. It needed to connect by JDBC to any database to execute some query that could be customized via command line parameters.

## 1.3. For pragmatic users who just want to download and use CheckByJdbc ##

All you need to do to get started is:

  * Download all files recursively from the CheckByJdbc project
  * Download [JSAP](http://www.martiansoftware.com/jsap/)-2.0a.jar (or newer) into the CheckByJdbc directory
  * Download the JDBC drivers for your database(s) and copy them into the CheckByJdbc directory. Any JDBC drivers included in this repository are likely outdated, so downloading new drivers from an official site is recommended.

    - Oracle:
        + classes12.jar
    - MySql: 
        + mysql-connector-java-5.1.18-bin
    - Microsoft SQL Server: 
        + sqljdbc4.jar (Windows)
        + sqljdbc_4.0.2206.100_enu.tar (Linux)
    - IBM DB2/400 (aka DB2 for i):
        + jt400.jar ( http://jt400.sourceforge.net/ )
    - HP Vertica:
        + vertica-jdbc-*x.x.x-x*.jar ([HP Vertica Client Drivers](http://www.vertica.com/resources/vertica-client-drivers/))
    - IBM DB2 for LUW, z/OS, and i (depending on license JAR)
        + db2jcc.jar ([DB2 JDBC Driver Versions and Downloads](http://www-01.ibm.com/support/docview.wss?uid=swg21363866))
        + db2jcc_license_cu.jar (or db2jcc_license_cisuz.jar for midrange or mainframe DB2 databases)