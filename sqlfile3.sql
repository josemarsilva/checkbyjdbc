select case
         when last_call_et  > &c then 
              2 /* CRITICAL */
         when last_call_et  > &w then 
              1 /* WARNING  */
         else 0 /* OK       */
       end nagios_return_value,
       '[Sid=' || sid || ',Serial#=' || serial# || ',SqlHash=' || sql_hash_value || ', LastCallEt=' || last_call_et nagios_msg_status
from   v$session a
where  username is not null
and    status = 'ACTIVE'
and    last_call_et > &w
