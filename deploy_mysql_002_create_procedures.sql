/*
 * ----------------------------------------------------------------------------
 * File Name    : deploy_mysql_002_create_procedures.sql
 * Description  : Sample procedure for check_by_jdbc
 * ----------------------------------------------------------------------------
 */

# mysql -u nagios -pnagios;
mysql> use nagios_dbtest;
mysql> drop procedure check_by_jdbc_sample;

delimiter //
create procedure check_by_jdbc_sample
  (
    OUT num_ret         INT,
    OUT str_ret         VARCHAR(1000),
    IN  w_param         INT,
    IN  c_param         INT,
    IN  str_adhoc_param VARCHAR(1000)
  )
BEGIN
  SET num_ret = 0;
  SET str_ret = 'CheckByJdbc-MySQL procedure: Your input parameter value was "". I have evaluated this and the return to Nagios is !' ;

  /* 
   * Nagios return constants ..
   */
  SET @NAGIOS_RETURN_OK       = 0;
  SET @NAGIOS_RETURN_WARNING  = 1;
  SET @NAGIOS_RETURN_CRITICAL = 2;
  SET @NAGIOS_RETURN_UNKNOWN  = 3;  

  /*
   * Put your code here! Your code should evaluate something, considering "w"arning, "c"ritical 
   * parameters and "str_adhoc_param" a string ad-hoc parameter and return on "num_ret" Nagios 
   * STATUS convention and "str_ret" Nagios STATUS return string
   */
  SET @to_char_NAGIOS_RETURN_OK = CAST( @NAGIOS_RETURN_OK AS CHAR);
  SET @to_char_c_param = CONVERT(c_param, UNSIGNED INTEGER);
  SET @to_char_w_param = CONVERT(w_param, UNSIGNED INTEGER);
  if str_adhoc_param = @to_char_NAGIOS_RETURN_OK then
    SET num_ret = @NAGIOS_RETURN_OK;
  elseif str_adhoc_param = @to_char_c_param then
    SET num_ret = @NAGIOS_RETURN_CRITICAL;
  elseif str_adhoc_param = @to_char_w_param then
    SET num_ret = @NAGIOS_RETURN_WARNING;
  else
    SET num_ret = @NAGIOS_RETURN_UNKNOWN;
  end if;
  SET str_ret = CONCAT('CheckByJdbc MySql Stored Procedure Sample: Your input parameter value was "', str_adhoc_param, '". I have evaluated this and the return to Nagios is ', CAST(num_ret AS CHAR), '!' );
END //
DELIMITER ;
