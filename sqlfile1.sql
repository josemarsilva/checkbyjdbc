select case
         when round(((df.bytes - fs.bytes) / df.bytes) * 100) > &c then 
              2 /* CRITICAL */
         when round(((df.bytes - fs.bytes) / df.bytes) * 100) > &w then
              1 /* WARNING  */
         else 0 /* OK       */
       end nagios_return_value,
       '[Tbs: '  || df.tablespace_name || ', ' ||
       'Max: '   || round((df.BYTES + fs.bytes) / 1024 /1024 ) || 'mb, ' ||
       'Used: '  || (df.bytes / 1024 / 1024 ) || 'mb, ' ||
       '%Used: ' || round(((df.bytes - fs.bytes) / df.bytes) * 100) || '% ]' nagios_status_msg
from   (
         select tablespace_name,
                sum(bytes) bytes,
                count(distinct file_id) file_id
         from dba_data_files
         group by tablespace_name
       ) df,
       (
         select tablespace_name,
                sum(bytes) bytes
         from   dba_free_space
         group  by tablespace_name
       ) fs
 where df.tablespace_name = fs.tablespace_name
 order by df.tablespace_name asc
 
