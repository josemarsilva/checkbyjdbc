/* 
 * ----------------------------------------------------------------------------
 * File Name    : deploy_mysql.sql
 * Description  : Main install script for MySQL Samples application
 * ----------------------------------------------------------------------------
 */

create database nagios_dbtest;

create user nagios@'%' identified by 'nagios';
create user nagios@'localhost' identified by 'nagios';

grant select on *.* to 'nagios'@'%';
grant select on *.* to 'nagios'@'localhost';

grant all on nagios_dbtest.* to 'nagios'@'%';
grant all on nagios_dbtest.* to 'nagios'@'localhost';

flush privileges;
