-- ----------------------------------------------------------------------------
-- File Name    : deploy_orcl.sql
-- Description  : Main install script for Oracle Samples
-- ----------------------------------------------------------------------------
set echo on

@@deploy_orcl_001_create_user
@@deploy_orcl_002_create_procedures

